package mk.plugin.vip.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import mk.plugin.vip.gui.VIPGUI;
import mk.plugin.vip.main.MainVIP;
import mk.plugin.vip.util.VIPUtils;
import mk.plugin.vip.vip.VIPCoins;

public class Listeners implements Listener {
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		VIPUtils.removePrefix(player);
		VIPUtils.saveData(player, VIPUtils.getData(player));
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Bukkit.getScheduler().runTaskLater(MainVIP.plugin, () -> {
			Player player = e.getPlayer();
			VIPUtils.removePermissions(player);
			VIPUtils.setDefault(player);
			VIPUtils.reload(player);
			VIPUtils.checkBug(player);
			if (VIPUtils.getTimeRemain(player) <= 0) {
				player.sendMessage("§aBạn đã hết hạn VIP");
				return;
			}
			VIPUtils.setPermissions(player);
		}, 20);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		VIPGUI.onClick(e);
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Player player = e.getPlayer();
			ItemStack item = player.getInventory().getItemInMainHand();
			if (item.isSimilar(VIPCoins.ITEM)) {
				e.setCancelled(true);
				if (VIPUtils.getData(player).getVIP().getLevel() >= 1) {
					if (item.getAmount() == 1) player.getInventory().setItemInMainHand(null);
					item.setAmount(item.getAmount() - 1);
					Bukkit.getScheduler().runTask(MainVIP.plugin, () -> {
						VIPCoins.runCommand(player);
					});
				}
				else {
					player.sendMessage("§cChỉ VIP mới có thể dùng");
				}
			}
		}
	}
	
}
