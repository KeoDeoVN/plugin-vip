package mk.plugin.vip.vip;

import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Lists;

public enum VIP {
	
	DEFAULT(0),
	VIP1(1),
	VIP2(2),
	VIP3(3),
	VIP4(4),
	VIP5(5),
	VIP6(6),
	VIP7(7),
	VIP8(8),
	VIP9(9),
	VIP10(10);
	
	private int level;
	private String prefix;
	private int expTo;
	private VIP copyOf;
	private List<String> permissions;
	private List<String> desc;
	
	private VIP(int level) {
		this.level = level;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public String getPrefix() {
		return this.prefix;
	}
	
	public int getExpTo() {
		return this.expTo;
	}
	
	public VIP getCopyOf() {
		return this.copyOf;
	}
	
	public List<String> getPermissions() {
		return this.permissions;
	}
	
	public List<String> getDesc() {
		return this.desc;
	}
	
	public void load(FileConfiguration config) {
		this.prefix = config.getString("vip." + this.name().toLowerCase() + ".prefix").replace("&", "§");
		this.expTo = config.getInt("vip." + this.name().toLowerCase() + ".exp-to");
		String copy = config.getString("vip." + this.name().toLowerCase() + ".copy");
		if (copy != null && copy.length() > 0) {
			this.copyOf = VIP.valueOf(copy.toUpperCase());
		}
		this.permissions = config.getStringList("vip." + this.name().toLowerCase() + ".permissions");
		this.permissions.add("prefix.1." + this.prefix.replace("§", "&"));
		this.desc = Lists.newArrayList();
		config.getStringList("vip." + this.name().toLowerCase() + ".desc").forEach(s -> {
			this.desc.add(s.replace("&", "§"));
		});
	}
	
	public static void loadAll(FileConfiguration config) {
		for (VIP vip : values()) {
			vip.load(config);
		}
	}
	
}
