package mk.plugin.vip.vip;

import org.bukkit.configuration.file.FileConfiguration;

public class VIPExtends {
	
	public static int COST;
	public static long TIME_EXTEND;
	public static String TIME_DISPLAY;
	public static int EXP;
	
	public static void reload(FileConfiguration config) {
		COST = config.getInt("vip-extend.cost");
		TIME_EXTEND = config.getLong("vip-extend.time-extend");
		TIME_DISPLAY = config.getString("vip-extend.time-display");
		EXP = config.getInt("vip-extend.exp");
	}
	
	
}
