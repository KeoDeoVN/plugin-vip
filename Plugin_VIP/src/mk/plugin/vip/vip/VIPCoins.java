package mk.plugin.vip.vip;

import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mk.plugin.vip.itembuilder.ItemBuilder;

public class VIPCoins {
	
	public static ItemStack ITEM;
	public static List<String> COMMANDS;
	
	public static void reload(FileConfiguration config) {
		ITEM = ItemBuilder.buildItem(config.getConfigurationSection("vip-coin.item"));
		COMMANDS = config.getStringList("vip-coin.rewards");
	}
	
	public static void runCommand(Player player) {
		String cmd = COMMANDS.get(new Random().nextInt(COMMANDS.size()));
		for (String s : cmd.split(";")) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s.replace("%player%", player.getName()));
		}
	}
	
}
