package mk.plugin.vip.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.vip.main.MainVIP;
import mk.plugin.vip.vip.VIPCoins;

public class AdminCommands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if (!sender.hasPermission("vip.*")) return false;
		
		if (args.length == 0) {
			sendTut(sender);
		}
		
		else if (args[0].equalsIgnoreCase("reload")) {
			MainVIP.plugin.reload();
			sender.sendMessage("Reloaded");
		}
		
		else if (args[0].equalsIgnoreCase("getcoin")) {
			Player player = (Player) sender;
			player.getInventory().addItem(VIPCoins.ITEM);
			sender.sendMessage("Okay");
		}
		
		return false;
	}

	public void sendTut(CommandSender sender) {
		sender.sendMessage("§a/vips reload");
		sender.sendMessage("§a/vips getcoin");
	}
	
}
