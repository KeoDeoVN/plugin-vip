package mk.plugin.vip.gui;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import mk.plugin.vip.main.MainVIP;
import mk.plugin.vip.player.VIPPlayer;
import mk.plugin.vip.pointapi.PointAPI;
import mk.plugin.vip.util.VIPUtils;
import mk.plugin.vip.vip.VIP;
import mk.plugin.vip.vip.VIPExtends;

public class VIPGUI {
	
	public static final int INFO_SLOT = 2;
	public static final int BUY_SLOT = 4;
	public static final int TUT_SLOT = 6;
	public static final List<Integer> VIP_SLOTS = Lists.newArrayList(9, 10, 11, 12, 13, 14, 15, 16, 17, 22);
	
	public static final String TITLE = "§0§lSORASKY | VIP";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 27, TITLE);
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainVIP.plugin, () -> {
			player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ; i++) inv.setItem(i, getBlackSlot());
			inv.setItem(INFO_SLOT, getInfoSlot(player));
			inv.setItem(BUY_SLOT, getBuySlot(player));
			inv.setItem(TUT_SLOT, getTutSlot(player));
			for (int i = 0 ; i < VIP_SLOTS.size() ; i++) {
				inv.setItem(VIP_SLOTS.get(i), getVIPSlot(VIP.values()[i + 1]));
			}
		});
	}
	
	public static void onClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().equalsIgnoreCase(TITLE)) return;
		e.setCancelled(true);
		int slot = e.getSlot();
		Player player = (Player) e.getWhoClicked();
		if (slot == BUY_SLOT) {
			if (!PointAPI.pointCost(player, VIPExtends.COST)) {
				player.sendMessage("§cLượng point của bạn không đủ để thực hiện việc này");
				return;
			}
			player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
			player.sendMessage("§aGia hạn thành công!");
			
			VIPUtils.removePermissions(player);
			VIPUtils.removePrefix(player);
			
			VIPPlayer vp = VIPUtils.getData(player);
			vp.addTimeExpired(VIPExtends.TIME_EXTEND);
			vp.addVIPExp(VIPExtends.EXP);
			VIPUtils.saveData(player, vp);
			
			VIPUtils.setPermissions(player);
			VIPUtils.setPrefix(player);
			
			e.getInventory().setItem(INFO_SLOT, getInfoSlot(player));
		}
	}
	
	public static ItemStack getBlackSlot() {
		ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE);
		item.setDurability((short) 15);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(" ");
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack getInfoSlot(Player player) {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("§c§lThông tin");
		VIPPlayer vp = VIPUtils.getData(player);
		List<String> lore = Lists.newArrayList();
		lore.add(getVIPString(player));
		lore.add("§aRank hiện tại: §f" + vp.getVIP().getPrefix());
		lore.add("§aThời gian còn lại: §f" + VIPUtils.format(VIPUtils.getTimeRemain(player)));
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack getBuySlot(Player player) {
		ItemStack item = new ItemStack(Material.REDSTONE_BLOCK);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("§c§lGia hạn VIP");
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick để gia hạn");
		lore.add("§a+ " + VIPExtends.EXP + " Exp VIP");
		lore.add("§a+ " + VIPExtends.TIME_DISPLAY + " VIP");
		lore.add("§c- §e" + VIPExtends.COST + "P");
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack getTutSlot(Player player) {
		ItemStack item = new ItemStack(Material.BOOK);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("§c§lHướng dẫn");
		List<String> lore = Lists.newArrayList();
		
		lore.add("§7§oGia hạn VIP để nhận exp và tăng");
		lore.add("§7§ohạn VIP. Lên cấp VIP thì quyền");
		lore.add("§7§olợi càng tăng");
		
		meta.setLore(lore);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack getVIPSlot(VIP vip) {
		ItemStack item = new ItemStack(Material.BOOK_AND_QUILL);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("§6§lVIP§e§l+" + vip.getLevel());
		meta.setLore(vip.getDesc());
		item.setItemMeta(meta);
		return item;
	}
	
	public static String getVIPString(Player player) {
		VIPPlayer vp = VIPUtils.getData(player);
		VIP c = vp.getVIP();
		VIP next = VIPUtils.getNextVIP(c);
		List<String> contents = Lists.newArrayList();
		for (int i = 0 ; i < 8 ; i++ ) contents.add("∎");
		int cE = vp.getVIPExp() - VIPUtils.getExpTo(c);
		int max = next.getExpTo();
		contents.add(cE + "");
		contents.add("/");
		contents.add(max + "");
		for (int i = 0 ; i < 8 ; i++ ) contents.add("∎");
		int amount = Math.min(contents.size(), Math.max(contents.size() * cE / max, 1));	
		String r = "";
		for (int i = 0 ; i < amount ; i++) {
			r += "§a" + contents.get(i);
		}
		for (int i = amount ; i < contents.size() ; i++) {
			r += "§7" + contents.get(i);
		}
		r = c.getPrefix() + " §r" + r + "§r " + next.getPrefix();
		return r;
	}
	
	
	
	
	
}
