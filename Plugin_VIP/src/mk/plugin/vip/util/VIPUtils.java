package mk.plugin.vip.util;

import org.bukkit.entity.Player;

import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;
import mk.plugin.vip.player.VIPPlayer;
import mk.plugin.vip.vip.VIP;
import mk.plugin.vip.vip.VIPExtends;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;

public class VIPUtils {
	
	public static VIPPlayer getData(Player player) {
		PlayerData pd = PlayerDataAPI.getPlayerData(player);
		int vipExp = 0;
		if (pd.hasData("vip.vipExp")) vipExp = Integer.valueOf(pd.getValue("vip.vipExp"));
		long timeExpired = System.currentTimeMillis();
		if (pd.hasData("vip.timeExpired")) timeExpired = Long.valueOf(pd.getValue("vip.timeExpired"));
		timeExpired = Math.max(System.currentTimeMillis() - 1, timeExpired);

		return new VIPPlayer(vipExp, timeExpired);
	}
	
	public static void saveData(Player player, VIPPlayer vp) {
		PlayerData pd = PlayerDataAPI.getPlayerData(player);
		pd.set("vip.vipExp", vp.getVIPExp() + "");
		pd.set("vip.timeExpired", vp.getTimeExpired() + "");
	}
	
	public static void removePrefix(Player player) {
//		player.setDisplayName(player.getDisplayName().replace(VIPUtils.getData(player).getVIP().getPrefix(), ""));
	}
	
	public static void setPrefix(Player player) {
//		player.setDisplayName(VIPUtils.getData(player).getVIP().getPrefix() + player.getDisplayName());
	}
	
	public static void removePermissions(Player player) {
		User user = LuckPermsProvider.get().getUserManager().getUser(player.getUniqueId());
		for (VIP vip : VIP.values()) {
			vip.getPermissions().forEach(perm -> {
				user.data().remove(Node.builder(perm).build());
			});
			user.data().remove(Node.builder("vip." + vip.getLevel()).build());
		}
		LuckPermsProvider.get().getUserManager().saveUser(user);
	}
	
	public static void setDefault(Player player) {
		VIP vip = VIP.DEFAULT;
		User user = LuckPermsProvider.get().getUserManager().getUser(player.getUniqueId());
		user.data().add(Node.builder("vip." + vip.getLevel()).build());
		for (int i = 0 ; i <= vip.getLevel() ; i++) {
			VIP v = getVIP(i);
			int ci = i;
			v.getPermissions().forEach(perm -> {
				if (perm.contains(v.getPrefix().replace("§", "&"))) {
					if (ci != vip.getLevel()) return;
				}
				user.data().add(Node.builder(perm).build());
			});
		}
		LuckPermsProvider.get().getUserManager().saveUser(user);
	}
	
	public static void setPermissions(Player player) {
		User user = LuckPermsProvider.get().getUserManager().getUser(player.getUniqueId());
		VIP vip = getData(player).getVIP();
		user.data().add(Node.builder("vip." + vip.getLevel()).build());
		vip.getPermissions().forEach(perm -> {
			user.data().add(Node.builder(perm).build());
		});
		LuckPermsProvider.get().getUserManager().saveUser(user);
	}
	
	public static void reload(Player player) {
		VIPUtils.removePermissions(player);
		VIPUtils.removePrefix(player);
		
		VIPPlayer vp = VIPUtils.getData(player);
		vp.addTimeExpired(0);
		vp.addVIPExp(0);
		VIPUtils.saveData(player, vp);
		
		VIPUtils.setPermissions(player);
		VIPUtils.setPrefix(player);
	}
	
	public static int getExpTo(VIP vip) {
		int sum = 0;
		for (VIP v : VIP.values()) {
			sum += v.getExpTo();
			if (v == vip) break;
		}
		return sum;
	}
	
	public static VIP getNextVIP(VIP vip) {
		for (int i = 0 ; i < VIP.values().length ; i++) {
			if (i == VIP.values().length - 1) break;
			if (VIP.values()[i] == vip) {
				return VIP.values()[i + 1];
			}
		}
		return VIP.VIP10;
	}
	
	public static VIP getPreviosVIP(VIP vip) {
		for (int i = 0 ; i < VIP.values().length ; i++) {
			if (VIP.values()[i] == vip) {
				if (i == 0) return VIP.DEFAULT;
				return VIP.values()[i - 1];
			}
		}
		return VIP.DEFAULT;
	}
	
	public static VIP getVIP(int level) {
		for (VIP v : VIP.values()) {
			if (v.getLevel() == level) return v;
		}
		return null;
	}
	
	public static long getTimeRemain(Player player) {
		VIPPlayer vp = VIPUtils.getData(player);
		return Math.max(0, vp.getTimeExpired() - System.currentTimeMillis());
	}
	
	public static String format(long miliTime) {
		return (miliTime / (3600000 * 24)) + "d " + ((miliTime % (3600000 * 24)) / 3600000) + "h " + ((miliTime % 3600000) / 60000) + "m";
	}
	
	public static void checkBug(Player player) {
		if (getTimeRemain(player) > get10Year()) {
			VIPPlayer vp = getData(player);
			long right = System.currentTimeMillis() + vp.getVIPExp() / VIPExtends.EXP * VIPExtends.TIME_EXTEND - (15 * 86400000);
			vp.setTimeExpired(right);
			System.out.println("Oh shittt");
			VIPUtils.saveData(player, vp);
		}
	}
	
	private static long get10Year() {
		return (long) 1000 * 60 * 60 * 24 * 365 * 10;
	}
	
//	private static long getTimePlayed(Player player) {
//		return player.getStatistic(Statistic.PLAY_ONE_TICK) / 20 * 1000;
//	}
	
}
