package mk.plugin.vip.player;

import mk.plugin.vip.util.VIPUtils;
import mk.plugin.vip.vip.VIP;

public class VIPPlayer {
	
	private long timeExpired;
	private int vipExp;
	
	public VIPPlayer(int vipExp, long timeExpired) {
		this.vipExp = vipExp;
		this.timeExpired = timeExpired;
	}
	
	public int getVIPExp() {
		return this.vipExp;
	}
	
	public void setVIPExp(int exp) {
		this.vipExp = exp;
	}
	
	public void addVIPExp(int exp) {
		this.setVIPExp(this.getVIPExp() + exp);
	}
	
	public VIP getVIP() {
		if (this.timeExpired < System.currentTimeMillis()) return VIP.DEFAULT;
		for (int i = 1 ; i < VIP.values().length ; i++) {
			int sum = VIPUtils.getExpTo(VIP.values()[i]);
			if (vipExp < sum) return VIP.values()[i - 1];
		}
		return VIP.VIP10;
	}
	
	public long getTimeExpired() {
		return this.timeExpired;
	}
	
	public void setTimeExpired(long time) {
		this.timeExpired = time;
	}
	
	public void addTimeExpired(long time) {
		this.setTimeExpired(this.getTimeExpired() + time);
	}
	
}
