package mk.plugin.vip.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.vip.command.AdminCommands;
import mk.plugin.vip.command.PlayerCommands;
import mk.plugin.vip.listener.Listeners;
import mk.plugin.vip.vip.VIP;
import mk.plugin.vip.vip.VIPCoins;
import mk.plugin.vip.vip.VIPExtends;
import mk.plugin.vip.yaml.YamlConfig;

public class MainVIP extends JavaPlugin {
	
	public static MainVIP plugin;
	
	@Override
	public void onEnable() {
		this.initInstance();
		this.reload();
		this.registerEvents();
		this.registerCommands();
	}
	
	public void initInstance() {
		plugin = this;
	}
	
	public void reload() {
		this.saveDefaultConfig();
		YamlConfig.reloadAll(this);
		VIP.loadAll(YamlConfig.CONFIG.get());
		VIPCoins.reload(YamlConfig.CONFIG.get());
		VIPExtends.reload(YamlConfig.CONFIG.get());
	}
	
	public void registerEvents() {
		Bukkit.getPluginManager().registerEvents(new Listeners(), this);
	}
	
	public void registerCommands() {
		this.getCommand("vip").setExecutor(new PlayerCommands());
		this.getCommand("vips").setExecutor(new AdminCommands());
	}
	
}
